$(function () {
    var swiper, swiper2, swiper3, swiper4, swiper5;

    $('.slider-info').each(function () {
        var $this = $(this);
        var sliderContainer = $this.find('.swiper-container');
        swiper = new Swiper(sliderContainer, {
            pagination: '.swiper-pagination',
            paginationClickable: true,
            loop: true,
            nextButton: $this.find('.swiper-button-next'),
            prevButton: $this.find('.swiper-button-prev'),
            spaceBetween: 30
        });
    });


    $('.pr-slider .list').each(function () {
        var $this = $(this);
        var sliderContainer = $this.find('.swiper-container');
        swiper4 = new Swiper(sliderContainer,
                {
                    slidesPerView: 'auto',
                    observer: true,
                    loop: true,
                    nextButton: $this.find('.swiper-button-next'),
                    prevButton: $this.find('.swiper-button-prev'),
                    spaceBetween: 0
                });
    });

    $('.slider-simple').each(function () {
        var $this = $(this);
        var sliderContainer = $this.find('.swiper-container');
        swiper2 = new Swiper(sliderContainer, {
            slidesPerView: 'auto',
            observer: true,
            loop: true,
            nextButton: $this.find('.swiper-button-next'),
            prevButton: $this.find('.swiper-button-prev'),
            spaceBetween: 30
        });
    });

    $('.section-facade__brand-1').each(function () {
        var $this = $(this);
        var sliderContainer = $this.find('.swiper-container');
        swiper3 = new Swiper(sliderContainer, {
            direction: 'vertical',
            slidesPerView: 13,
            height: 500,
            nextButton: $this.find('.swiper-button-next'),
            prevButton: $this.find('.swiper-button-prev'),
            spaceBetween: 0
        });
    });

    $('.section-facade__brand-2').each(function () {
        var $this = $(this);
        var sliderContainer = $this.find('.swiper-container');
        swiper5 = new Swiper(sliderContainer, {
            direction: 'vertical',
            slidesPerView: 5,
            height: 450,
            nextButton: $this.find('.swiper-button-next'),
            prevButton: $this.find('.swiper-button-prev'),
            spaceBetween: 10
        });
    });


    $('.fancy-popup-gallery').fancybox({
        wrapCSS: 'fancy-gallery',
        mouseWheel: false,
        padding: 0,
        helpers: {
            overlay: {
                locked: false
            }
        },
        //padding: 59,
        tpl: {
            closeBtn: '<a title="Close" class="red_close btn_pop_close" href="javascript:;"><i class="icon-close"></i></a>',
            next: '<a title="Next" class="fancybox-nav fancybox-next" href="javascript:;"><span><i class="icon-arrow-r"></i></span></a>',
            prev: '<a title="Prev" class="fancybox-nav fancybox-prev" href="javascript:;"><span><i class="icon-arrow-l"></i></span></a>'
        }
    });

    $('.fancy-popup').fancybox(
            {
                wrapCSS: 'fancy-popup',
                mouseWheel: false,
                padding: 0,
                helpers: {
                    overlay: {
                        locked: false
                    }
                },
                tpl: {
                    closeBtn: '<a title="Close" class="red_close btn_pop_close" href="javascript:;"><i class="icon-close"></i></a>',
                }
            });



    $('.tab-head li').on('click', function () {
        $('.tab-head li').removeClass('active');
        $(this).addClass('active');

        var index = $(this).index();

        $('.tab-body .col').removeClass('active').eq(index).addClass('active');
        $('.list-images__i').removeClass('open').eq(index).addClass('open');

    });


    var stickyHeaders = (function () {
        var $window = $(window),
                $stickies;
        var load = function (stickies) {
            if (typeof stickies === "object" && stickies instanceof jQuery && stickies.length > 0) {
                $stickies = stickies.each(function () {
                    var $thisSticky = $(this).wrap('<div class="followWrap" />');
                    $thisSticky
                            .data('originalPosition', $thisSticky.offset().top)
                            .data('originalHeight', $thisSticky.height())
                            .parent()
                            .height($thisSticky.height());
                });
                $window.off("scroll.stickies").on("scroll.stickies", function () {
                    _whenScrolling();
                });
            }
        };

        var _whenScrolling = function () {
            $stickies.each(function (i) {
                var $thisSticky = $(this),
                        $stickyPosition = $thisSticky.data('originalPosition');
                if ($stickyPosition <= $window.scrollTop() + $thisSticky.data('originalHeight') + 45) {
                    var $nextSticky = $stickies.eq(i + 1),
                            $nextStickyPosition = $nextSticky.data('originalPosition') - $thisSticky.data('originalHeight');
                    $thisSticky.addClass("fixed");
                    $('.menu-p').css('top', '75px');
                    if ($nextSticky.length > 0 && $thisSticky.offset().top >= $nextStickyPosition) {
                        // $thisSticky.addClass("absolute").css("top", $nextStickyPosition);
                    }
                } else {

                    var $prevSticky = $stickies.eq(i - 1);
                    $thisSticky.removeClass("fixed");
                    $('.menu-p').css('top', '0');
                    if ($prevSticky.length > 0 && $window.scrollTop() <= $thisSticky.data('originalPosition')) {
                        $prevSticky.removeClass("absolute").removeAttr("style");

                    }
                }
            });
        };

        return {
            load: load
        };
    })();

    $(function () {
        stickyHeaders.load($(".followMeBar"));
    });

    $(window).off("scroll").on("scroll", function () {
        if ($(window).scrollTop() > 70) {
            $('.header-f').addClass('active');
        } else {
            $('.header-f').removeClass('active');
        }
    });

    var $selector = $(".wrap-imgas");
    $selector.mCustomScrollbar({
        axis: "x"
    });

    $(document).on('click', '.switch a', function (e) {
        e.preventDefault();
    });

    $(document).on('click', '.switch_i a:not(.active)', function (e) {
        e.preventDefault();
        var $this = $(this);
        $this.closest('.switch').find('.tumbler').toggleClass('active');
        $this.closest('.switch').find('.switch-i').toggleClass('active');
        var $img = $this.closest('.section-sel').find('.section-sel__img .img');
        $img.removeClass('active');

        if ($('.switch_i .switch-i-l').hasClass('active')) {
            $img.eq(0).addClass('active');
        } else {
            $img.eq(1).addClass('active');
        }
    });

    $(document).on('click', '.switch__list a:not(.active)', function (e) {
        e.preventDefault();
        var $this = $(this);
        $this.closest('.switch').find('.tumbler').toggleClass('active');
        $this.closest('.switch').find('.switch-i').toggleClass('active');

        var list = $this.closest('.section-sel').find('.list__li');

        if ($('.switch__list .switch-i-l').hasClass('active')) {
            list.eq(0).addClass('active');
            list.eq(1).removeClass('active');
        } else {
            list.eq(1).addClass('active');
            list.eq(0).removeClass('active');
        }

        $('.pr-slider .list').each(function () {
            var $this = $(this);
            var sliderContainer = $this.find('.swiper-container');
            swiper4 = new Swiper(sliderContainer,
                    {
                        slidesPerView: 4,
                        nextButton: $this.find('.swiper-button-next'),
                        prevButton: $this.find('.swiper-button-prev'),
                        spaceBetween: 0
                    });
        });
    });

    $(document).on('click', '.wrap-imgas .link-sel a', function (e) {
        e.preventDefault();
        var $this = $(this),
                index = $(this).index();
        $('.wrap-imgas .link-sel a').removeClass('active');
        $this.addClass('active');
        $('.wrap-imgas .link-sel__body .list').removeClass('active').eq(index).addClass('active');
    });

    $('.col-sl a').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        $('.col-sl a').removeClass('active');
        $this.addClass('active');
        $this.closest('.section-facade').find('.wrap-imgas__i, .find-brand .text').fadeOut();
        setTimeout(function () {
            $this.closest('.section-facade').find('.wrap-imgas__i, .find-brand .text').remove();
        }, 400);
        $.ajax({
            url: $(this).attr('href'), success: function (data) {
                setTimeout(function () {
                    $this.closest('.section-facade').find('.wrap-imgas').fadeIn();
                }, 400);
                setTimeout(function () {
                    $this.closest('.section-facade').find('.wrap-imgas').append(data);
                    var text = $this.closest('.section-facade').find('.find-brand .text');
                    $this.closest('.section-facade').find('.find-brand .text').remove();
                    $this.closest('.section-facade').find('.item').prepend(text);
                }, 500);
                $selector.mCustomScrollbar('destroy');
                setTimeout(function () {
                    $selector.mCustomScrollbar({
                        axis: "x"
                    });
                }, 500);
            }
        });
        return false;
    });



    $(document).on("scroll", onScroll);

    //smoothscroll
    $('.nav li a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('.nav li').each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');

        var target = this.hash,
                menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top + 2
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });

    function onScroll(event) {
        var scrollPos = $(document).scrollTop();
        $('.nav li a').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href"));
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('.nav li').removeClass("active");
                currLink.closest('li').addClass("active");
            } else {
                currLink.closest('li').removeClass("active");
            }
        });
    }
    ;

// Cache selectors
    var lastId,
            topMenu = $(".nav"),
            topMenuHeight = topMenu.outerHeight() + 15,
            // All list items
            menuItems = topMenu.find("a"),
            // Anchors corresponding to menu items
            scrollItems = menuItems.map(function () {
                var item = $($(this).attr("href"));
                if (item.length) {
                    return item;
                }
            });

// Bind click handler to menu items
// so we can get a fancy scroll animation
    menuItems.click(function (e) {
        var href = $(this).attr("href"),
                offsetTop = href === "#" ? 0 : $(href).offset().top - topMenuHeight + 1;
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 300);
        e.preventDefault();
    });


// Bind to scroll
    $(window).scroll(function () {
        // Get container scroll position
        var fromTop = $(this).scrollTop() + topMenuHeight;

        // Get id of current scroll item
        var cur = scrollItems.map(function () {
            if ($(this).offset().top < fromTop)
                return this;
        });
        // Get the id of the current element
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
                    .parent().removeClass("active")
                    .end().filter("[href='#" + id + "']").parent().addClass("active");
        }
    });


    $('.nav li a, .icons a[href^="#"]').on('click', function (e) {
        if ($(window).width() < 768) {
            e.preventDefault();
            var index = $(this).attr('href');
            $('.dropdown-nav').removeClass('active');
            $('.btn-open-nav').removeClass('active');
            setTimeout(function () {
                $('html, body').animate({
                    scrollTop: $(index).offset().top
                }, 1000);
            }, 400);
        } else {
            e.preventDefault();
            var index = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(index).offset().top - 70
            }, 1000);
        }
    });

    $('form.lead a.btn').on('click', function (e) {
        e.preventDefault();
        var $form = $(this).parents('form'),
                data = $form.serialize(),
                btn = $form.find('a.btn');

        $form.find('.rfield').each(function () {
            if ($(this).val() !== '') {
                $(this).removeClass('empty_field');
            } else {
                $(this).addClass('empty_field');
            }
        });

        var sizeEmpty = $form.find('.empty_field').length;
        if (sizeEmpty > 0) {
            btn.addClass('disabled');
        } else {
            btn.removeClass('disabled');
        }

        if ($(this).hasClass('disabled')) {
            $form.find('.empty_field').addClass('rf_error');
            $form.find('.empty_field').parents('.row-inp').find('.rfield_error').css({'visibility': 'visible'});
            setTimeout(function () {
                $form.find('.empty_field').removeClass('rf_error');
                $form.find('.empty_field').parents('.row-inp').find('.rfield_error').css({'visibility': 'hidden'});
            }, 2000);
            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: location.origin + '/ajax/ajax_form.php',
                dataType: 'json',
                data: data,
                beforeSend: function (data) {
                    $(this).attr('disabled', 'disabled');
                },
                success: function (data) {
                },
                error: function (xhr, ajaxOptions, thrownError) {
                },
                complete: function (data) {
                    $form[0].reset();
                    $.fancybox.close();
                    $('.fancy-popup').fancybox(
                            {
                                wrapCSS: 'fancy-popup',
                                mouseWheel: false,
                                padding: 0,
                                helpers: {
                                    overlay: {
                                        locked: false
                                    }
                                },
                                tpl: {
                                    closeBtn: '<a title="Close" class="red_close btn_pop_close" href="javascript:;"><i class="icon-close"></i></a>',
                                }
                            });
                    $.fancybox($('#popup-end'), {
                        wrapCSS: 'fancy-popup popup-end',
                        tpl: {
                            closeBtn: '<a title="Close" class="red_close btn_pop_close" href="javascript:;"><i class="icon-close"></i></a>'
                        },
                        mouseWheel: false,
                        padding: 0,
                        helpers: {
                            overlay: {
                                locked: false
                            }
                        }
                    });
                }
            });
            return false;
        }
    });
    $('.popup-end .btn_pop_close').on('click', function () {
        $.fancybox.close();
    });
});
